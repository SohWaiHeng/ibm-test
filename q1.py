shirt_num = int(input())
shirts = input().split(' ')
req_num = int(input())
sizes = input().split(' ')

for i, s in enumerate(shirts):
    if s[-1] == 'S':
        shirts[i] = len(s) * -1
    elif s[-1] == 'L':
        shirts[i] = len(s)
    else:
        shirts[i] = 0

for i, s in enumerate(sizes):
    if s[-1] == 'S':
        sizes[i] = len(s) * -1
    elif s[-1] == 'L':
        sizes[i] = len(s)
    else:
        sizes[i] = 0

shirts.sort()
sizes.sort()
valid = True
for i in range(len(sizes)):
    if sizes[i] > shirts[i]:
        print("No")
        valid = False
if valid:
    print("Yes")

