SELECT O.owner_id, O.owner_name, B.different_category_count
FROM owner O
JOIN (
    SELECT DISTINCT COUNT(category_id) AS different_category_count, A.owner_id AS owner_id
    FROM article A, category C, category_article_mapping CHECKSUM_AGG
    WHERE CAM.category_id = C.category_id AND CAM.article_id = A.article_id
    GROUP BY A.owner_id
) B
ON B.owner_id = O.owner_id