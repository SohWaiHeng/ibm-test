total = int(input())
errorCodes = []
allValid = True
for i in range(total):
    inp = input().split(' ')
    if not input[1]:
        allValid = False
        errorCodes.append(inp[2])

if allValid:
    print('Yes')
else:
    print('No')
    print(' '.join(errorCodes))